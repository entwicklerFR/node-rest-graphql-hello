'use strict';

/* settings */
const proxyPort = 3000;
const mongooseURL = 'mongodb://localhost:27017/nrghDB';

/* npm modules */
const express = require('express');
const mongoose = require('mongoose');
const graphqlHTTP = require('express-graphql');
const graphql = require('graphql');

const app = express();

/* to support JSON-encoded and URL-encoded bodies */
app.use(express.json());
app.use(express.urlencoded({'extended': true}));

/* we add two api routers */
var restApiRouter = express.Router();
var graphqlApiRouter = express.Router();
app.use('/api/rest', restApiRouter);
app.use('/api/graphql', graphqlApiRouter);

/* ============================== */
/* ==== MONGOOSE DEFINITIONS ==== */
/* ============================== */
/* create a mongoose schema */
var productSchema = mongoose.Schema({
  name: String,
  price: Number,
  desc: String
});

/* create a mongoose model */
var productModel = mongoose.model('Product', productSchema);

/* remove function : returns a promise */
var removeAllProducts = function () {
  console.log('removing all products');
  return productModel.remove(); // promise (see mongoose doc)
};

/* save function : returns a promise */
var saveProduct = function (pProduct) {
  console.log('saving new product', JSON.stringify(pProduct));
  let newProduct = new productModel();
  newProduct.name = pProduct.name;
  newProduct.price = pProduct.price;
  newProduct.desc = pProduct.desc;  
  return newProduct.save(); // promise (see mongoose doc)
};

/* find function : returns a promise */
var listProducts = function (pQuery) {
  console.log('listing products');
  let query = pQuery ? pQuery : {};
  let projection = { '_id': 0, '__v': 0 };
  return productModel.find(query, projection).exec(); // promise (see mongoose doc)
};

/* ============================== */
/* ==== API REST DEFINITIONS ==== */
/* ============================== */
/* our first REST API method */
restApiRouter.get('/', function (req, res) {
  res.status(200).json({ 'message': 'this is our first REST API method, server is running' });
  return;
});

/* get products list : REST API method */
restApiRouter.get('/products', async function (req, res) {
  let products = [];
  try {
    products = await listProducts();
    res.status(200).json(products);
  } catch (err) {
    res.status(500).json(err); // displays error content : dangerous for security reason : only for demo purpose
  }
  return;
});

/* create a product : REST API method */
restApiRouter.post('/products', async function (req, res) {
  try {
    let product = await saveProduct(req.body);
    res.status(200).json(product);
  } catch (err) {
    res.status(500).json(err); // displays error content : dangerous for security reason : only for demo purpose
  }
  return;
});

/* ============================== */
/* == API GRAPHQL DEFINITIONS  == */
/* ============================== */
/* our GraphQL API */
// == 1 == create GraphQL schema
let schema = graphql.buildSchema(`
type Product {
  name: String!
  price: Int
  desc: String
}

input ProductInput {
  name: String!
  price: Int
  desc: String
}

type Query {
  message: String,
  products(name: String): [Product]
}

type Mutation  {
  saveProduct(input: ProductInput): Product
}  
`);

// == 2 == our resolvers
var root = {
  message: () => { return 'this is our first GraphQL API method, server is running'; },
  products: async ({ name }) => {
    return await listProducts(name ? { 'name': name } : {});
  },
  saveProduct: async ({ input }) => {
    return await saveProduct(input);
  },
};

graphqlApiRouter.use('/', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

/* ============================== */
/* ====== STARTING WEB PROXY ==== */
/* ============================== */
/* let's start ! */
// == 1 ==
mongoose.connect(mongooseURL, function (error) {
  if (error) {
    console.log('FAILED : Unable to connect to MongoDB [%s]', mongooseURL);
    console.log('ABORTED : API REST and GraphQL server not started');
    process.exit(0);
  }
  else {
    console.log('SUCCEED : Connected to MongoDB [%s]', mongooseURL);

    // == 2 == 
    app.listen(proxyPort, async function () {
      console.log('SUCCEED : API REST and GrapQL server started on port [%d]', proxyPort);

      // == 3 ==
      try { await removeAllProducts(); }
      catch (err) { console.log(err); }

      try { await saveProduct({ 'name': 'desktop', 'price': 1000, 'desc': 'gamer desktop' }); }
      catch (err) { console.log(err); }
      try { await saveProduct({ 'name': 'laptop', 'price': 1500, 'desc': 'gamer laptop' }); }
      catch (err) { console.log(err); }
    });
  }
});