# Simple implementation of a REST and GraphQL API embedded in a Node/Express server connected to a MongoDB backend
The project's purpose is to show how to implement a very simple GraphQL API based on the GraphQL Schema Language.  
To learn more about this description language you could read this excellent page: https://wehavefaces.net/graphql-shorthand-notation-cheatsheet-17cd715861b6.  

To improve the comparison between REST and GraphQL, I did the following things:  

+ each GraphQL method (Query and Mutation) has its own corresponding REST method (GET and POST respectively)
+ the whole code is written in a single file

```mermaid
graph TD;
  Client-->REST;
  Client-->GraphQL;
  REST-->Node/Express/Mongoose;
  GraphQL-->Node/Express/Mongoose;
  Node/Express/Mongoose-->MongoDB;
```

# Usage
I assume you have already installed Node and MongoDB, just:  

1. Clone the project or simply copy `package.json` and `server.js` files
2. Run `npm install`
3. Start your MongoDB server with `mongod`
4. Start the project with `npm start`

Depending on what method you want to test, use simply your browser (REST GET method and Graphiql tool) or your preferred installed REST Client.

For more details, you can see the following post on my blog : https://entwicklerfr.gitlab.io/2018/04/node---graphql/ (choose your preferred language if it's enabled).